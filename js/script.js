'use strict'

// Теорія:
// 
//      1. Для обработки взаимодействия пользователя с элементом input существует одноименное событие (input),
//      которое стоит использовать для этого действия.


const buttons = document.querySelectorAll(".btn");

document.addEventListener("keypress", function (event) {
    document.querySelector(`#${event.code}`).focus();
});